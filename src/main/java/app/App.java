package app;

import com.typesafe.config.Config;

import app.configurations.FluxConfiguration;
import app.configurations.StreamsConfiguration;
import app.processors.PriceStreamProcessor;
import io.jooby.CorsHandler;
import io.jooby.Jooby;
import io.jooby.ServerSentMessage;

public class App extends Jooby {
  {
    Config conf = getEnvironment().getConfig();
    
    FluxConfiguration fluxConfiguration = new FluxConfiguration();
    StreamsConfiguration streamsConfiguration = new StreamsConfiguration(conf.getString("bootstrapServers"));
    
    new PriceStreamProcessor(
      streamsConfiguration.getProperties(),
      conf.getString("inputTopic"),
      fluxConfiguration.getSink()
    ).start();

    decorator(new CorsHandler());
    get("/", ctx -> "Hello Jooby!");
    sse("/stream", sse -> fluxConfiguration.getFlux().subscribe(message -> sse.send(new ServerSentMessage(message))));
  }

  public static void main(final String[] args) {
    runApp(args, App::new);
  }
}
