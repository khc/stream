package app.configurations;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

public class FluxConfiguration {
  final Sinks.Many<String> sink;
  final Flux<String> flux;
  
  public FluxConfiguration() {
    sink = Sinks.many().unicast().onBackpressureBuffer();
    flux = sink.asFlux();
  }

  public Sinks.Many<String> getSink() {
    return sink;
  }

  public Flux<String> getFlux() {
    return flux;
  }
}
