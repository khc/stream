package app.processors;

import java.util.Properties;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;

abstract class StreamProcessor {
  protected KafkaStreams streams;
  protected StreamsBuilder builder;
  protected Properties streamsConfiguration;

  protected Serde<String> stringSerde = Serdes.String();
  protected Serde<Long> longSerde = Serdes.Long();

  public StreamProcessor(Properties streamsConfiguration) {
    this.streamsConfiguration = streamsConfiguration;
    builder = new StreamsBuilder();
  }
 
  protected void createStream(StreamsBuilder builder) {} 

  public void start() {
    createStream(builder);
    streams = new KafkaStreams(builder.build(), streamsConfiguration);
    streams.cleanUp();
    streams.start();
  }
}
