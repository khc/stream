package app.processors;

import java.util.Properties;

import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.Stores;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Sinks;

@Slf4j
public class PriceStreamProcessor extends StreamProcessor implements StreamProcessorInterface {
  final String inputTopic;
  final Sinks.Many<String> sink;

  public PriceStreamProcessor(Properties streamsConfiguration, String inputTopic, Sinks.Many<String> sink) {
    super(streamsConfiguration);
    this.inputTopic = inputTopic;
    this.sink = sink;
  }

  public void createStream(StreamsBuilder builder) {
    final KStream<String, String> stream = builder.stream(inputTopic);
    
    KeyValueBytesStoreSupplier storeSupplier =  Stores.persistentKeyValueStore("state-store");
    final KTable<String, String> table = stream.toTable(Materialized.as(storeSupplier));

    table.toStream().foreach((key,value) -> {
      log.info("### received {}, {}", key, value);
      sink.tryEmitNext(value);
    } );
  }
}
