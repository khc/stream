package app.processors;

import org.apache.kafka.streams.StreamsBuilder;

public interface StreamProcessorInterface {
  public void createStream(StreamsBuilder builder);
}
