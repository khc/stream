# stream

Welcome to stream!!

## running

    mvn clean jooby:run

## building

    mvn clean package

## docker

     docker build . -t stream
     docker run -p 8080:8080 -it stream
