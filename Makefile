run:
	@mvn clean jooby:run

build:
	@mvn clean package

image:
	@docker build . -t stream